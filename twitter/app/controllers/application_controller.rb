class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  
  def L4
    session[:co] ||=0
    session[:co] = session[:co].to_i+1
    render text: session[:co]
  end
end
